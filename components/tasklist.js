import React from 'react'
import { Row, Col, ListGroup, ListGroupItem, Button } from 'reactstrap'

export default class extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      tasks: props.tasks
    }
    this.onClickAdd = this.onClickAdd.bind(this);
    this.onClickItem = this.onClickItem.bind(this);
    this.onClickDeleteItem = this.onClickDeleteItem.bind(this);
  }

  onClickAdd = () => {
    console.log("add task")
    let tasks = this.state.tasks
    let newId

    (tasks.length === 0) ? newId = 1 : newId = tasks.length + 1

    tasks.push({id:newId, title:'Task new '+ newId })
    this.setState({ tasks: tasks})
  }

  onClickItem = (e) => {
    e.preventDefault()
    console.log(e.target.value)
    console.log('show item')
  }

  onClickDeleteItem = (e) => {
    let newTasks
    e.preventDefault()
    var value = e.target.value
    console.log(e.target.value)
    console.log('delete item')

    newTasks = this.state.tasks

    newTasks.forEach(function(t, index) {
      if (t.id == value) {
        let removedItem = newTasks.splice(index, 1)
        
      }
    })
    
    this.setState({ tasks: newTasks })
  }

  getButton = (id) => {
    return(
      <div key={ id } className="btnDelete">
        <Button key={ id } value={ id } onClick={ this.onClickDeleteItem } outline color="danger">delete</Button>
      </div>
    )
  }

  render() {
    return (
      <div>
      <Row className="m-3">
        <Col>
          <Button color="primary" onClick={ this.onClickAdd } active={ false }>Add</Button>{' '}
        </Col>
      </Row>
      <Row>
        <Col xs="7">
          <ListGroup>
            { this.state.tasks.map((task) => 
              <ListGroupItem key={ task.id } tag="button" href="#" onClick={ this.onClickItem } value={ task.id } action>{ task.title }</ListGroupItem>
              
            )}
          </ListGroup>
        </Col>
        <Col xs="5">
          {
            this.state.tasks.map((task) => 
              this.getButton(task.id)
            )
          }
        </Col>
      </Row>
      </div>
    )
  }
}