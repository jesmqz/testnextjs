import React from "react"
import Layout from "../components/layout"
import { Container,
         ListGroup, 
         ListGroupItem,
        Button, Row, Col } from "reactstrap"

import TaskList from "../components/tasklist"

export default class TaskPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tasks: []
    }
    
  }



  render() {
    return(
      <Layout>
        <Container>
          <TaskList tasks={ this.state.tasks }/>
        </Container>
      </Layout>
    )
  }
}