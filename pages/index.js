import React from 'react'
import Layout from '../components/layout'

import {
    Container,
    Jumbotron, 
    Button } from 'reactstrap';
  

class MainPage extends React.Component {
    static async getInitialProps({ req }) {
      const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
      return { userAgent }
    }
    constructor(props) {
        super(props);
      }

      render() {
        return (
          <Layout>
            <Container>
              <Jumbotron>
                <h1 className="display-3">Test, step!</h1>
                <p className="lead">This is a simple hero unit, a simple Jumbotron-style component for calling extra attention to featured content or information.</p>
                <hr className="my-2" />
                <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
                <p className="lead">
                  <Button color="primary">Learn More</Button>
                </p>
              </Jumbotron>
            </Container>
          </Layout>
        );
      }
  }
  
export default MainPage